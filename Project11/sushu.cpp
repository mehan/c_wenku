#define _CRT_SECURE_NO_WARNINGS  1

#include <stdio.h>

//打印100-200之间的素数；
int is_prim(int x) {
	for (int j = 2	; j < x; j++)
	{
		if (x % j == 0) {
			return 0;
		}
	}
	return 1;
}



void main() {
	int i = 0;
	int count = 0;
	for ( i = 100; i <=200; i++)
	{
	
	
		if (is_prim(i)==1)
		{
			count++;
			printf("%d\t", i);
			if (count % 5 == 0)

			{
				printf("\n");
			}
		}
	}	
	
	printf("\n\t共计count=%d个值", count);
}
	