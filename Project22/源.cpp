#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<malloc.h>
#include<stdlib.h>
typedef struct LNode {
    int    data;
    struct LNode* next;
}LNode, * LinkList;
/* 尾插法 */
void creatListTailInsert(LinkList& L, int n) {
    LinkList p, tailPointer;
    int i;//计数
    L = (LinkList)malloc(sizeof(LNode));
    if (!L)  exit(0);            //分配空间失败则退出程序
    L = NULL; //no headcrunode
    tailPointer = L; //把尾赋给尾指针
    printf("taillist(%d):", n);
    for (i = 0; i < n; i++) {
        p = (LinkList)malloc(sizeof(LNode));
        if (!p)  exit(0);
        scanf("%d", &(p->data));
        if (L == NULL)    L = p;      //当链表为空,L赋给第一个结点
        else tailPointer->next = p;  //将新结点插入尾部；
        p->next = NULL;
        tailPointer = p; //插入的结点变为尾结点
    }
}
/* 头插法 */
void creatListHeadInsert(LinkList& L, int n) {
    LinkList p;
    int i;//计数
    L = (LinkList)malloc(sizeof(LNode));
    if (!L)  exit(0);            //分配空间失败则退出程序
    L = NULL; //no headcrunode
    printf("headlist(%d):", n);
    for (i = 0; i < n; i++) {
        //创建新结点
        p = (LinkList)malloc(sizeof(LNode));
        if (!p)  exit(0);
        scanf("%d", &(p->data));
        if (L != NULL)   p->next = L;
        else    p->next = NULL;
        L = p;     //将头结点 next指向赋给新结点
    }
}
/* 依次显示表中所有元素 */
void getAllElem(LinkList& L, int n) {
    LinkList    p;
    int i = 0;
    p = L;
    while (p && i < n) {
        printf("%d ", p->data);
        p = p->next;
        i++;
    }
    printf("\n");
}
void main() {
    LinkList    headList;
    LinkList    tailList;
    int count;  //插入元素个数
    printf("count=");
    scanf("%d", &count);
    creatListHeadInsert(headList, count);
    printf("tailList:");
    getAllElem(tailList, count);
}
