#define _CRT_SECURE_NO_WARNINGS  1
#include<stdio.h>

void   sawp(int* pa	, int *pb) {
	int tmp = 0;
	tmp = *pa;
	*pa = *pb;
	*pb = tmp;
}

void main() {
	int a = 0; int b = 0;
	printf("请输入两个数进行交换");
	scanf("%d %d", &a, &b);
	printf("交换前的值为：%d %d\n", a, b);
	sawp(&a, &b);
	printf("交换后的值为：%d %d", a, b);

}