#define _CRT_SECURE_NO_WARNINGS  1
#include <stdio.h>
void main() {
	int arr[] = { 1,2,3,4,5,6,7,8,9 };
	int k = 7;
	int se = sizeof(arr) / sizeof(arr[0]);
	int left = 0;
	int right = se;
	while (left<=right)
	{
		int mid = (left + right) / 2;
		if (arr[mid] < k) {
			left = mid -1;
		}
		else if (arr[mid]>k) 
		{
			right = mid - 1;
		}
		else
		{
			printf("找到了，下标是：%d", mid);
			break;
		}
		if (left > right) {
			printf("找不到");
		}
	}

}
