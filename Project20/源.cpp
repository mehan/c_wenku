#define _CRT_SECURE_NO_WARNINGS 1
#include "stdio.h"
#include "string.h"
#include "windows.h"


typedef struct
{
	char p_name[10];//进程名
	char p_pro;//优先级 1-3个级别 1.低 2.中 3.高
	char p_status;//运行状态 0.阻塞 1.运行 2.就绪
	int p_runtime;//运行时间
	int p_wait;//等待时间
	struct PCB* next;//指针,指向下一个PCB
}PCB;

void Run(PCB* head)//任何时候保证程序里面至少有一个进程在运行
{
	PCB* p = head->next;//直接将P指向第一个结点
	while (p != NULL)//遍历一遍链表,将所有就绪队列等待时间加1,防止前面结点因为唤醒又进入运行状态
	{
		if (p->p_status == '2')
		{
			p->p_wait++;//将等待时间加1
		}
		p = p->next;
	}

	p = head->next;//将P重置在第一个结点
	while (p->p_status != '1' && p != NULL)
	{
		if (p->p_status == '2')//防止线性链表前面的结点因为从阻塞唤醒后又进入运行状态
		{
			p->p_status = '1';
			p->p_wait = 2;
		}
		if (p->p_status == '1')//对上一个if进行处理
		{
			return;
		}
		p = p->next;
	}
	return;
}

void Insert(PCB* head, PCB* temp)//插入链表函数
{
	PCB* p;
	p = head;//将头结点保存起来
	while (p->next != NULL)
	{
		p = p->next;
	}

	p->next = temp;
	temp->next = NULL;
}

int Check(PCB* head, PCB* temp)
{
	PCB* p = head;
	while (p->next)
	{
		p = p->next;
		if (strcmp(p->p_name, temp->p_name) == 0)
			return 0;
	}
	return 1;
}

void Create(PCB* head)//创建进程函数
{
	int chk = 0;
	PCB* temp;//申请临时存储空间,方便接受数据
	temp = (PCB*)malloc(sizeof(PCB));
	system("cls");
	printf("\t----------进程创建-----------\n");
	printf("\n请输入进程名:");
	scanf("%s", temp->p_name);
	getchar();
	/*检查进程名称,如果相同则返回主界面*/
	chk = Check(head, temp);
	if (chk == 0)
	{
		printf("进程队列已有该名称进程,创建失败，即将返回主界面.\n");
		system("pause");
		return;
	}
	printf("\n请输入进程优先级(1.低 2.中 3.高):");
	scanf("%c", &temp->p_pro);
	getchar();
	printf("\n请输入进程运行时间:");
	scanf("%d", &temp->p_runtime);
	getchar();
	temp->p_status = '2';
	temp->p_wait = 2;
	/*
	printf("\n请输入该进程状态:");
	scanf("%c",&temp->p_status);
	getchar();
	*/
	Insert(head, temp);//调用插入链表函数
	system("pause");
	Run(head);
}

void Show(PCB* head)//显示队列进程函数
{
	int ready = 1, block = 1, run = 1;
	PCB* p = head, * q;
	system("cls");
	if (p->next == NULL)
	{
		printf("目前系统中没有进程.请返回主界面创建进程!\n");
		system("pause");
		return;
	}

	/*列出就绪队列列表*/
	q = p->next;//指针指到第一个结点
	printf("\n--就绪队列--\n");
	while (q)
	{
		if (q->p_status == '2')
		{
			printf("%d)进程名:%s", ready++, q->p_name);
			printf("  进程优先级:%c", q->p_pro);
			printf("  进程运行时间:%d", q->p_runtime);
			printf("  进程等待时间:%d\n", q->p_wait);
		}
		q = q->next;
	}
	printf("\n");

	/*列出运行队列列表*/
	q = p->next;//将指针重置到第一个结点
	printf("\n--运行队列--\n");
	while (q)
	{
		if (q->p_status == '1')
		{
			printf("%d)进程名:%s", run++, q->p_name);
			printf("  进程优先级:%c", q->p_pro);
			printf("  进程运行时间:%d\n", q->p_runtime);
			//printf("  进程已运行时间:");
		}
		q = q->next;
	}
	printf("\n");

	/*列出阻塞队列列表*/
	q = p->next;
	printf("\n--阻塞队列--\n");
	while (q)
	{
		if (q->p_status == '0')
		{
			printf("%d)进程名:%s", block++, q->p_name);
			printf("  进程优先级:%c", q->p_pro);
			printf("  进程运行时间:%d", q->p_runtime);
			printf("  进程等待时间:%d\n", q->p_wait);
		}
		q = q->next;
	}
	printf("\n");

	printf("进程显示完毕.");
	system("pause");
}

void Block(PCB* head)//阻塞进程函数
{
	char name[10];
	PCB* p = head;//保护头结点
	system("cls");
	printf("\t----------阻塞进程-----------\n");
	printf("\n输入你要放入阻塞队列的进程名称:");
	scanf("%s", name);
	getchar();
	p = p->next;
	while (p)
	{
		if (strcmp(p->p_name, name) == 0)
			break;
		p = p->next;
	}
	if (!p)
	{
		printf("\n队列中无该进程.\n");
		system("pause");
	}

	if (p->p_status == '1')
	{
		printf("\n该进程正在运行.\n");
		printf("\n将该进程放入阻塞队列\n\n");
		system("pause");
		p->p_status = '0';
		printf("\n该进程已经被放入阻塞队列\n");
		system("pause");
	}
	else
	{
		if (p->p_status == '0')
		{
			printf("\n该进程已在阻塞队列中.\n");
			system("pause");
		}
		if (p->p_status == '2')
		{
			printf("\n该进程正在就绪队列中.不可放入阻塞队列\n");
			system("pause");
		}
	}
	Run(head);
}

void Delete(PCB* head, PCB* temp)/*head为链表头结点,temp为将要删除的结点*/
{
	PCB* p = head, * q = temp->next;
	while (p->next != temp)
	{
		p = p->next;
	}
	p->next = q;
	free(temp);
}

void Stop(PCB* head)//终止进程函数
{
	char name[10];
	PCB* p = head;
	system("cls");
	printf("\t----------终止进程-----------\n");
	printf("\n输入你要终止的进程名称:");
	scanf("%s", name);
	getchar();
	p = p->next;
	while (p)
	{
		if (strcmp(p->p_name, name) == 0)
			break;
		p = p->next;
	}
	if (!p)
	{
		printf("进程队列中无该进程.\n");
		system("pause");
	}
	Delete(head, p);//调用删除结点函数
	printf("\n进程终止成功\n");
	system("pause");
	Run(head);
}

void Wakeup(PCB* head)//唤醒进程函数
{
	char name[10];
	PCB* p = head;//保护头结点
	system("cls");
	printf("\t----------唤醒进程-----------\n");
	printf("\n输入你要唤醒的进程名称:");
	scanf("%s", name);
	getchar();
	p = p->next;
	while (p)
	{
		if (strcmp(p->p_name, name) == 0)
			break;
		p = p->next;
	}
	if (!p)
	{
		printf("阻塞队列中无该进程名称.\n");
		system("pause");
		return;
	}
	if (p->p_status == '0')
	{
		printf("该进程正在阻塞队列中.\n");
		printf("\n将该进程放回就绪队列中\n");
		system("pause");
		p->p_status = '2';
		p->p_wait = 2;
		printf("\n该进程已经被放入就绪队列中\n");
		system("pause");
	}
	else
	{
		if (p->p_status == '1')
		{
			printf("\n该进程正在运行.不可唤醒\n");
			system("pause");
		}
		if (p->p_status == '2')
		{
			printf("\n该进程正在就绪队列中.不可唤醒\n");
			system("pause");
		}
	}
}

void prior_Sche(PCB* head)
{
	PCB* p = head->next, * temp = head->next;//保护头结点p,temp为将要删除的结点
	system("cls");
	if (p == NULL)
	{
		printf("目前系统中没有进程.请返回主界面创建进程!\n");
		system("pause");
		return;
	}
	while (p)
	{
		if (temp->p_pro < p->p_pro)
		{
			temp = p;//将此时优先级最大的结点地址给临时空间保存
		}
		p = p->next;
	}
	printf("\n\n");
	printf("经过调度,此时程序中运行的进程是:\n");
	printf("\n  进程名:%s", temp->p_name);
	printf("  进程优先级:%c", temp->p_pro);
	printf("  进程运行时间:%d\n", temp->p_runtime);
	printf("\n该进程PCB显示完毕!\n");
	system("pause");
	Delete(head, temp);
	Run(head);
}

void time_Sche(PCB* head)
{
	int ready = 1;
	PCB* p = head, * q, * temp = NULL;//保护头结点p,temp为时间片用完将要删除时，保护的临时结点
	system("cls");
	if (p->next == NULL)
	{
		printf("目前系统中没有进程.请返回主界面创建进程!\n");
		system("pause");
		return;
	}

	/*列出就绪队列列表*/
	q = p->next;//指针指到第一个结点
	printf("\n--就绪队列--\n");
	while (q)
	{
		if (q->p_status == '2')
		{
			printf("%d)进程名:%s", ready++, q->p_name);
			printf("  进程优先级:%c", q->p_pro);
			printf("  进程运行时间:%d\n", q->p_runtime--);
			//printf("  进程已运行时间:");
		}
		if (q->p_runtime == 0)
		{
			temp = q;
		}
		q = q->next;
	}
	if (temp != NULL)
	{
		Delete(head, temp);
	}
	printf("\n");
	system("pause");
}

void Scheduling(PCB* head)//调度程序
{
	while (1)
	{
		int choose;
		system("cls");
		printf("1.优先级调度\n");
		printf("2.时间片调度\n");
		printf("0.返回主菜单\n");
		printf("\n请输入选项:");
		scanf("%d", &choose);
		getchar();
		switch (choose)
		{
		case 1:prior_Sche(head); break;
		case 2:time_Sche(head); break;
		case 0: { system("cls"); return; }break;
		default: {printf("请输入0-2的数字\n"); system("pause"); system("cls"); }break;
		}
	}
}

void Menu()
{
	printf("\t----------模拟系统进程创建、终止、阻塞、唤醒-----------");
	printf("\n");
	printf("1.进程创建\n");
	printf("2.阻塞进程\n");
	printf("3.唤醒进程\n");
	printf("4.终止进程\n");
	printf("5.显示进程\n");
	printf("6.调度进程\n");
	printf("0.退出\n");
	printf("\n\n");
	printf("\t------------------------完美分割线---------------------\n");
	printf("功能介绍:\n");
	printf("阻塞:运行->阻塞;处于运行之外状态,给出提示信息;若进程不存在也给出其他信息\n");
	printf("唤醒:根据输入的进程名结束进程;不管该进程处于什么状态都将结束;若输入进程不存在,会给出相应信息\n");
	printf("终止:根据输入的进程名结束进程;不管该进程处于什么状态都将结束;若输入进程不存在,会给出相应信息\n");
	printf("显示:分别显示就绪队列、阻塞队列、正在运行队列\n\n\n");
}

main()
{
	PCB* head;
	head = (PCB*)malloc(sizeof(PCB));//头结点为空
	head->next = NULL;
	if (!head)
	{
		printf("程序有误.即将退出");
		system("pause");
		exit(0);
	}
	while (1)
	{
		int choose;
		system("cls");
		Menu();
		printf("请选择使用功能:");
		scanf("%d", &choose);
		switch (choose)
		{
		case 1:Create(head); break;
		case 2:Block(head); break;
		case 3:Wakeup(head); break;
		case 4:Stop(head); break;
		case 5:Show(head); break;
		case 6:Scheduling(head); break;
		case 0:exit(0); break;
		default: {printf("请输入0-5的数字\n"); system("pause"); system("cls"); }break;//输入'.'会造成bug.
		}
	}
}
