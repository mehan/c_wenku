#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>    //本实验中使用到的库函数
#include <stdlib.h>
#include <string.h>
int max[5][1];        //开始定义银行家算法中需要用到的数据
int allocation[5][1];
int need[5][1];
int available[1];
int request[5][1];
char* finish[5];
int safe[5];
int n, i, m;
int k = 0;
int j = 0;
int work[1];
int works[5][1];
void line()     //美化程序，使程序运行时更加明朗美观
{
	printf("------------------------------------------------\n");
}
void start()   //表示银行家算法开始
{
	line();
	printf("                         银行家算法开始\n");
	printf("                                   --  死锁避免方法  \n");
	line();
}
void end()     //表示银行家算法结束
{
	line();
	printf("                     银行家算法结束，谢谢使用\n");
	line();
}
void input()  //输入银行家算法起始各项数据
{
	for (n = 0; n < 5; n++)
	{
		printf("请输入进程P%d的相关信息：\n", n);
		printf("Max:");
		for (m = 0; m < 1; m++)
			scanf("%d", &max[n][m]);
		printf("Allocation:");
		for (m = 0; m < 1; m++)
			scanf("%d", &allocation[n][m]);
		for (m = 0; m < 1; m++)
			need[n][m] = max[n][m] - allocation[n][m];
	}
	printf("请输入系统可利用资源数Available:");
	for (m = 0; m < 1; m++)
		scanf("%d", &available[m]);
}
void output()  //输出系统现有资源情况
{
	line();
	printf("资源情况    Max     Allocation    Need      Available\n");
	printf("进程        A         A           A          A  \n");
	line();
	for (n = 0; n < 5; n++)
	{
		printf("P%d%3d%3d%3d", n, max[n][0], allocation[n][0], need[n][0]);
		if (n == 0)
			printf("%3d%3d\n", available[0]);
		else
			printf("\n");
	}
	line();
}
void change()   //当Request[i,j]<=Available[j]时，系统把资源分配给进程P[i]，Available[j]和Need[i,j]发生改变
{
	for (m = 0; m < 1; m++)
	{
		available[m] -= request[i][m];
		allocation[i][m] += request[i][m];
		need[i][m] -= request[i][m];
	}
}
void outputsafe()   //输出安全序列的资源分配表
{
	printf("该安全序列的资源分配图如下：\n");
	line();
	printf("资源情况   Work       Need    Allocation Work+Allocation  Finish\n");
	printf("进程        A          A        A           A  \n");
	line();
	for (n = 0; n < 5; n++)
		printf("P%d%9d%3d%3d%5d%12s\n", safe[n], works[safe[n]][0], need[safe[n]][0], allocation[safe[n]][0], works[safe[n]][0] + allocation[safe[n]][0], finish[n]);
	line();
}
int check()    //安全性算法
{
	printf("开始执行安全性算法……\n");
	for (m = 0; m < 1; m++)    //数组work和finish初始化
		work[m] = available[m];
	for (n = 0; n < 5; n++)
	{
		finish[n] == "false";
		safe[n] = 0;
	}
	k = 0;
	for (m = 0; m < 5; m++)
		for (n = 0; n < 5; n++)
			if (strcmp(finish[n], "false") == 0 && need[n][0] <= work[0])   //查找可以分配资源但尚未分配到资源的进程
			{
				safe[k] = n;  //以数组safe[k]记下各个进程得到分配的资源的顺序
				works[safe[k]][0] = work[0];
				work[0] += allocation[n][0];   //进程执行后释放出分配给它的资源
				finish[n] == "ture"; //finish[n]变为1以示该进程完成本次分
				k++;
			}
	for (m = 0; m < 5; m++)  //判断是否所有进程分配资源完成
	{
		if (strcmp(finish[m], "false") == 0)
		{
			printf("找不到安全序列，系统处于不安全状态。\n");
			return 0;    //找不到安全序列，结束check函数，返回0
		}
		else
			if (m == 4)     //此处m=4表示所有数组finish的所有元素都为ture
			{
				printf("找到安全序列P%d->P%d->P%d->P%d->P%d，系统是安全的\n", safe[0], safe[1], safe[2], safe[3], safe[4]);
				j = 1;
				outputsafe();     //输出安全序列的资源分配表
			}
	}
	return 1;
}
void main()  //主程序开始
{
	start();
	for (; j == 0;)     //确认输入数据的正确性，若输入错误，重新输入
	{
		input();
		printf("以下为进程资源情况，请确认其是否正确：\n");
		output();
		printf("数据是否无误：\n正确：输入1\n错误：输入0\n请输入：");
		scanf("%d", &j);
	}
	printf("数据确认无误，算法继续。\n");
	if (check() == 0)     //若check函数返回值为0，表示输入的初始数据找不到安全序列，无法进行下一步，程序结束
	{
		end();
		exit(0);
	}
	for (; j == 1;)     //当有多个进程请求资源时，循环开始
	{
		printf("请输入请求资源的进程i(0、1、2、3、4)：");  //输入发出请求向量的进程及请求向量
		scanf("%d", &i);
		printf("请输入进程P%d的请求向量Request%d：", i, i);
		for (n = 0; n < 1; n++)
			scanf("%d", &request[i][n]);
		for (; request[i][0] > need[i][0];) //若请求向量大于需求资源，则认为是输入错误，要求重新输入
		{
			printf("数据输入有误，请重试！\n请输入进程P%d的请求向量Request%d：", i, i);
			for (n = 0; n < 1; n++)
				scanf("%d", &request[i][n]);
		}
		if (request[i][0] <= available[0])  //判断系统是否有足够资源提供分配
		{
			printf("系统正在为进程P%d分配资源……\n", i);
			change();    //分配资源
			j = 0;
		}
		else
			printf("系统没有足够的资源，进程P%d需要等待。\n", i);
		if (j == 0)   //j=0表示系统有足够资源分配的情况
		{
			printf("当前系统资源情况如下：\n");  //输出分配资源后的系统资源分配情况
			output();
			if (check() == 0)    //若找不到安全系列，则之前的资源分配无效
			{
				printf("本次资源分配作废，恢复原来的资源分配状态。\n");
				for (m = 0; m < 1; m++)    //恢复分配资源前的系统资源状态
				{
					available[m] += request[i][m];
					allocation[i][m] -= request[i][m];
					need[i][m] += request[i][m];
				}
				output();     //输出系统资源状态
			}
		}
		printf("是否还有进程请求资源？\n是：输入1\n否：输入0\n请输入：");
		scanf("%d", &j);      //若还有进程请求资源，j=1，之前的for循环条件满足
	}
	end();
}
