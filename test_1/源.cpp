#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

#define MAXLEN 30

typedef struct
{
	int elem[MAXLEN];
	int length;
}Sqlist;

void Create(Sqlist& L)//创建
{
	int n, i = 0;
	printf("请输入线性表元素个数（小于30）\n");
	while (scanf("%d", &n) && n > 30 || n < 0)
	{
		printf("数据不合法，请重新输入\n");
	}
	printf("请输入%d个数，用空格分开\n", n);
	while (n--)
	{
		scanf("%d", &L.elem[i]);
		i++;
		L.length++;
	}
}
void Show(Sqlist L)//显示
{
	printf("当前顺序表中元素有:\t");
	for (int i = 0; i < L.length; i++)
	{
		printf("%d ", L.elem[i]);
	}
	printf("\n");
}
int Insert(Sqlist& L, int i, int x)//插入
{
	int key = 1;
	if (i<1 || i>L.length + 1)
	{
		printf("插入位置不正确\n");
		key = 0;
		return key;
	}
	for (int j = L.length - 1; j >= i - 1; j--)
		L.elem[j + 1] = L.elem[j];
	L.elem[i - 1] = x;
	L.length++;
	return key;
}
int Delete(Sqlist& L, int i)//删除
{
	int key = 1;
	if (i<1 || i>L.length)
	{
		printf("删除位置不正确\n");
		key = 0;
		return key;
	}
	for (int j = i; j <= L.length - 1; j++)
		L.elem[j - 1] = L.elem[j];
	L.length--;
	return key;
}
int Search(Sqlist L, int e)//查找 
{
	int i, key = 1;
	for (i = 0; i < L.length && L.elem[i] != e; i++);
	if (i < L.length)
	{
		printf("查找成功，%d在", e);
		for (; i < L.length; ++i)
			if (e == L.elem[i])
				printf("第%d", i + 1);
		printf("的位置\n");
		return key;
	}
	else { printf("查找失败\n"); key = 0; return key; }
}
void DelSqlist(Sqlist& L)//去重 
{
	int i, j, temp;
	for (j = 0; j < L.length; j++)
		for (i = 0; i < L.length - 1 - j; i++)
		{
			if (L.elem[i] > L.elem[i + 1])
			{
				temp = L.elem[i];
				L.elem[i] = L.elem[i + 1];
				L.elem[i + 1] = temp;
			}
		}
	i = 0;
	while (i < L.length - 1)
		if (L.elem[i] == L.elem[i + 1])
		{
			for (j = i + 1; j < L.length; j++)
				L.elem[j - 1] = L.elem[j];
			L.length--;
		}
		else
			i++;
}

int main()
{
	int i, x, e, slect, key;
	Sqlist L;
	L.length = 0;
	Create(L);
	Show(L);
	printf("请选择操作:\t 1.插入元素\t2.删除元素\t3.查找元素\t4.删除重复元素\n");
	while (~scanf("%d", &slect))
	{
		key = 0;
		switch (slect)
		{
		case 1:if (L.length >= MAXLEN) {
			printf("顺序表中已放满元素，无法插入新元素\n");
			break;
		}
			  while (key == 0) {
				  printf("请输入要插到的位置和元素值\n");
				  scanf("%d%d", &i, &x);
				  key = Insert(L, i, x);
				  Show(L);
			  }break;
		case 2:while (key == 0) {
			printf("请输入要删除的位置\n");
			scanf("%d", &i);
			key = Delete(L, i);
			Show(L);
		}break;
		case 3: while (key == 0) {
			printf("请输入要查找的元素\n");
			scanf("%d", &e);
			key = Search(L, e);
		}break;
		case 4:DelSqlist(L); Show(L); break;
		default:printf("输入错误，请重新输入\n"); break;
		}
		printf("请选择操作:\t 1.插入\t2.删除\t3.查找\t4.删除重复元素\n");
	}
}
