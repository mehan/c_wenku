#define _CRT_SECURE_NO_WARNINGS  1
#include <stdio.h>

	int binary_serch(int a[],int k,int s) {
	int left = 0;
	int right = s - 1;
	

	while (left<=right)
	{
		int mid = (left + right) / 2;
		if (a[mid]<k) {

		left = mid + 1;
	}
		else if (a[mid]>k)
	{
		right = mid - 1;

	}	else
		{
			return mid;
		}

	}

	return -1;
}

	void main() {
		int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
		int sz = sizeof(arr) / sizeof(arr[0]);
		int	k = 7;
		//如果找到就返回下标。找不到就返回-1

		int ret = binary_serch(arr, k, sz);
	
		if (-1==ret)
		{
			printf("没找到");

		}
		else
		{
			printf("找到了，下标为：%d", ret);
		}
	}
