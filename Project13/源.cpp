#define _CRT_SECURE_NO_WARNINGS  1

#include <stdio.h>

//打印出1000-2000中的所以闰年

int is_true(int year) {

	if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
	{
		return 1;
	}
}


void main() {
	int count = 0;
	for ( int i = 1000; i <= 2000; i++)
			
	{
	
		if ((is_true(i) == 1)) {
			printf("%d\t", i);
			count++;
			if (count%10==0)
			{
				printf("\n");

			}
		}

	}

	printf("\n从1000-2000中共计有：%d个闰年",count);
	
}